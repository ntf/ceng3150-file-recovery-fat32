#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <exception>

#include <string>
#include <iostream>
#include <sstream>

#include <vector>
#include <map>

#include <sys/stat.h>
#include <fcntl.h>
#include <openssl/md5.h>
#include "fat.h"
#include "core.h"
using namespace std;
using namespace Recover;

Recover::Request::Request() {
	this->deviceFileName = NULL;
	this->longFileName = NULL;
	this->shortFileName = NULL;
	this->md5 = NULL;
}

void Recover::Core::handle(Request* req) {

	this->request = req;

	if (req->flags['i']) {
		//milestone 2
		this->openDevice(O_RDONLY);
		this->printFileSystemInfo();
	} else if (req->flags['l']) {
		//milestone 3
		this->openDevice(O_RDONLY);
		//this->printFileSystemInfo();
		this->listAllDirectoryEntries();
	} else if (req->flags['r'] || req->flags['R']) {
		// milestone 4
		this->openDevice(O_RDWR);
		this->recoverEntry();
	}

}

void Recover::Core::openDevice(int mode) {
	//read boot entry
	this->boot = new BootEntry;
	this->fd = open(this->request->deviceFileName, mode); //open the device file
	read(fd, this->boot, sizeof(BootEntry));

	//construct FAT
	this->fat = new FAT;
	this->fat->setFileDescriptor(fd);
	this->fat->initialize(boot);

	lseek(this->fd, 0, SEEK_SET);
}
void Recover::Core::printFileSystemInfo() {
	//BootEntry data;
	int dataStart = this->boot->BPBBytsPerSec
			* (this->boot->BPBRsvdSecCnt
					+ this->boot->BPBFATSz32 * this->boot->BPBNumFATs);

	int dataEnd = lseek(fd, 0, SEEK_END);
	int total = (dataEnd - dataStart)
			/ (this->boot->BPBBytsPerSec * this->boot->BPBSecPerClus);

	/*	printf("[DEBUG] data start %d ; end : %d ; total :%d\n", dataStart, dataEnd,
	 total);
	 printf("Cluster 2 at %d\n", this->getDataOffsetByClusterNo(2));
	 printf("Cluster 3 at %d : diff : %d \n", this->getDataOffsetByClusterNo(3), this->getDataOffsetByClusterNo(3) - this->getDataOffsetByClusterNo(2));
	 */
	/* (516608 / 4) - 2 > total clusters */
	int allocated = 0;
	int free = 0;
	int i;
	for (i = 2; i < total + 2; i++) {
		if (this->fat->addresses[i] != 0x0) {
			allocated++;
		} else {
			free++;
		}

	}

	printf("Number of FATs = %d\n", this->boot->BPBNumFATs);
	printf("Number of bytes per sector =  %d\n", this->boot->BPBBytsPerSec);
	printf("Number of sectors per cluster = %d\n", this->boot->BPBSecPerClus);
	printf("Number of reserved sectors = %d\n", this->boot->BPBRsvdSecCnt);
	printf("Number of allocated clusters = %d\n", allocated);
	printf("Number of free clusters = %d\n", free);
}

void Recover::Core::listAllDirectoryEntries() {

	//cluster 2 is start of root directory
	int clusterNo = 2;

	int count = 1;
	std::vector<LFNEntry> LFNStack;
	bool hasNext = false;

	do {
		//printf("start reading cluster No [%d]\n", clusterNo);
		int seekPos = lseek(this->fd, this->getDataOffsetByClusterNo(clusterNo),
		SEEK_SET);
		do {
			DirEntry dirData;
			LFNEntry LFNData;

			read(this->fd, &LFNData, sizeof(struct LFNEntry));

			if (LFNData.Attr == 0x0F) { //Only LFN's pos 11 (Attr) is 0x0F
				LFNStack.push_back(LFNData);
			} else if (LFNData.sequenceNumber != 0x00
					&& LFNData.sequenceNumber != 0x05
					&& LFNData.sequenceNumber != 0x2E
					&& LFNData.sequenceNumber != 0xE5) {
				//sequence number : first byte

				//move bytes on LFN to Dir
				memcpy(&dirData, &LFNData, sizeof(struct LFNEntry));

				//8.3 file name
				char dirName[10];
				memcpy(dirName, dirData.DIRName, 8);
				for (int k = 0; k < 8; k++) {
					if (dirName[k] == 32)
						dirName[k] = 0;
				}

				if (dirData.DIRAttr == 0x10) { //A directory
					dirName[8] = '/';
					dirName[9] = 0;
				} else {
					dirName[8] = 0;
				}

				int startingClusterNumber = this->getClusterNoFromHiLow(
						dirData.DIRFstClusHI, dirData.DIRFstClusLO);

				/*	int startingClusterNumber = 0;

				 char startingClusterNumberChar[5];
				 memcpy(&startingClusterNumberChar, &dirData.DIRFstClusLO, 2);
				 memcpy(&startingClusterNumberChar + 2, &dirData.DIRFstClusHI,
				 2);
				 memcpy(&startingClusterNumber, &startingClusterNumberChar, 4);
				 */
				if (LFNStack.size() > 0) {
					//printf("LFN Entry Count = %d\n", LFNStack.size());
					std::string longFileName = getLongFileName(LFNStack);
					if (dirData.DIRAttr == 0x10) { //A directory
						longFileName += '/';
					}

					LFNStack.clear();

					if (dirData.DIRNameExt[0] == 32
							&& dirData.DIRNameExt[1] == 32
							&& dirData.DIRNameExt[2] == 32) {
						//if no extension
						printf("%d, %s, %s, %d, %d\n", count, dirName,
								longFileName.c_str(), dirData.DIRFileSize,
								startingClusterNumber);
					} else {
						printf("%d, %s.%.3s, %s, %d, %d\n", count, dirName,
								dirData.DIRNameExt, longFileName.c_str(),
								dirData.DIRFileSize, startingClusterNumber);
					}
				} else {
					if (dirData.DIRNameExt[0] == 32
							&& dirData.DIRNameExt[1] == 32
							&& dirData.DIRNameExt[2] == 32) {
						//if no extension
						printf("%d, %s, %d, %d\n", count, dirName,
								dirData.DIRFileSize, startingClusterNumber);
					} else {
						printf("%d, %s.%.3s, %d, %d\n", count, dirName,
								dirData.DIRNameExt, dirData.DIRFileSize,
								startingClusterNumber);
					}
				}

				count++;
			} else {
				LFNStack.clear();
			}

			seekPos = lseek(fd, 0, SEEK_CUR);
		} while (seekPos + sizeof(struct DirEntry)
				<= this->getDataOffsetByClusterNo(clusterNo + 1));
		int nextCluster = this->fat->addresses[clusterNo];
		if (nextCluster > 0x0 && nextCluster <= 0x0ffffff7) {
			hasNext = true;
			//printf("%d 's next cluster is %d\n", clusterNo, nextCluster);
			clusterNo = nextCluster;
		} else {
			hasNext = false;
		}

	} while (hasNext);
}

int Recover::Core::getDataOffsetByClusterNo(int cluster) {
	int dataStart = this->boot->BPBBytsPerSec
			* (this->boot->BPBRsvdSecCnt
					+ this->boot->BPBFATSz32 * this->boot->BPBNumFATs);
	return dataStart
			+ this->boot->BPBBytsPerSec * this->boot->BPBSecPerClus
					* (cluster - 2);
}

std::string Recover::Core::getLongFileName(
		std::vector<Recover::LFNEntry> LFNStack) {
	std::stringstream ss;

//	printf("LFN SIZE=%d\n", LFNStack.size());
	for (std::vector<Recover::LFNEntry>::reverse_iterator it =
			LFNStack.rbegin(); it != LFNStack.rend(); ++it) {
		//printf("processing %x\n", it->sequenceNumber);
		//10
		for (int i = 0; i <= 10 - 2; i = i + 2) {
			if (it->Name1[i] == 0
					|| (it->Name1[i] == 255 && it->Name1[i + 1] == 255)) {

			} else {
				ss << it->Name1[i];
			}
		}
		//12
		for (int i = 0; i <= 12 - 2; i = i + 2) {
			if (it->Name2[i] == 0
					|| (it->Name2[i] == 255 && it->Name2[i + 1] == 255)) {

			} else {
				ss << it->Name2[i];
			}
		}
		//4
		for (int i = 0; i <= 4 - 2; i = i + 2) {
			if (it->Name3[i] == 0
					|| (it->Name3[i] == 255 && it->Name3[i + 1] == 255)) {

			} else {
				ss << it->Name3[i];
			}
		}
	}
	//	name += string(  reinterpret_cast<char*>(it->Name1) );
	//+ string(it->Name2) + string(it->Name3);
	char * cstr = new char[ss.str().length() + 1];
	std::strcpy(cstr, ss.str().c_str());
	//printf("%s\n", cstr);
	return ss.str();
}

int Recover::Core::getClusterNoFromHiLow(unsigned short hi,
		unsigned short low) {
	int clusterNo = 0;
	clusterNo = hi << 16;
	clusterNo = low;
	return clusterNo;
}

void Recover::Core::recoverEntry() {
	int i, j, k, m, n, r = 0;
	char* fileName;
	unsigned char parsedFileName[11], firstCharacterOfFileName;
	if (this->request->flags['R']) {
		firstCharacterOfFileName = toupper(this->request->longFileName[0]);
	} else if (this->request->flags['r']) {
		fileName = this->request->shortFileName;
		//initialize praseFileName with space
		for (i = 0; i < 11; i++)
			parsedFileName[i] = 0x20;

		i = j = 0;
		k = 8;
		while (*(fileName + i) != '\0') {
			if (i == 0) {
				firstCharacterOfFileName = *fileName;
				parsedFileName[0] = 0xe5;
			} else {
				if (*(fileName + i) == '.') {
					j = 1;
				} else {
					if (j == 0) {
						parsedFileName[i] = *(fileName + i);
					} else {
						parsedFileName[k] = *(fileName + i);
						k++;
					}
				}
			}
			i++;
		}

		//	printf("[Debug]FileName: %.11s\n", parsedFileName);

	}

	//cluster 2 is start of root directory
	int clusterNo = 2;
	int count = 1;
	bool hasNext = false;

	std::vector<RecoveryEntry*> foundEntry;

	do {
		//printf("start reading cluster No [%d]\n", clusterNo);
		int seekPos = lseek(this->fd, this->getDataOffsetByClusterNo(clusterNo),
		SEEK_SET);

		RecoveryEntry* recoveryEntry = new RecoveryEntry;
		do {

			//std::vector<LFNEntry> LFNStack;
			//std::vector<int> LFNStackPos;
			LFNEntry LFNData;

			read(this->fd, &LFNData, sizeof(struct LFNEntry));

			if (this->request->flags['R'] && LFNData.Attr == 0x0F) { //Only LFN's pos 11 (Attr) is 0x0F
				recoveryEntry->LFNStack.push_back(LFNData);
				recoveryEntry->LFNStackPos.push_back(seekPos);
			} else if (LFNData.sequenceNumber != 0x00
					&& LFNData.sequenceNumber != 0x05
					&& LFNData.sequenceNumber != 0x2E
					&& LFNData.sequenceNumber != 0xE5) {
				recoveryEntry->LFNStack.clear();
				recoveryEntry->LFNStackPos.clear();
			} else if (LFNData.sequenceNumber == 0xE5) {
				//sequence number : first byte
				//move bytes on LFN to Dir
				memcpy(&recoveryEntry->entry, &LFNData,
						sizeof(struct LFNEntry));

				int startingClusterNumber = this->getClusterNoFromHiLow(
						recoveryEntry->entry.DIRFstClusHI,
						recoveryEntry->entry.DIRFstClusLO);

				if (recoveryEntry->LFNStack.size() > 0) {
					//printf("LFN Entry Count = %d\n", LFNStack.size());
					std::string longFileName = getLongFileName(
							recoveryEntry->LFNStack);
					std::string requestedName(this->request->longFileName);

					//			cout << longFileName << "==" << requestedName << "\n";
					if (longFileName == requestedName) {
						recoveryEntry->pos = seekPos;

						foundEntry.push_back(recoveryEntry);
						recoveryEntry = new RecoveryEntry;
					}
					/*
					 printf("%d, %s, %d, %d\n", count, longFileName.c_str(),
					 recoveryEntry->entry.DIRFileSize,
					 startingClusterNumber); */
				} else {

					//8.3 file name
					char dirName[10];
					memcpy(dirName, recoveryEntry->entry.DIRName, 8);
					for (int k = 0; k < 8; k++) {
						if (dirName[k] == 32)
							dirName[k] = 0;
					}

					if (recoveryEntry->entry.DIRAttr == 0x10) { //A directory
						dirName[8] = '/';
						dirName[9] = 0;
					} else {
						dirName[8] = 0;
					}

					char dirFullName[11];
					memcpy(dirFullName, recoveryEntry->entry.DIRName, 8);
					memcpy(dirFullName + 8, recoveryEntry->entry.DIRNameExt, 3);

					if (memcmp(&parsedFileName, &dirFullName, 11) == 0) {

						if (this->request->flags['m']) {
							char* fileMD5 = new char[33];
							this->getMD5(recoveryEntry->entry, fileMD5);
							//compare md5 value
							//cout << this->request->md5 << " == " << fileMD5 << "\n";
							if (strcmp(this->request->md5, fileMD5) == 0) {
								recoveryEntry->pos = seekPos;
								foundEntry.push_back(recoveryEntry);
								recoveryEntry = new RecoveryEntry;
							}

						} else {
							recoveryEntry->pos = seekPos;
							foundEntry.push_back(recoveryEntry);
							recoveryEntry = new RecoveryEntry;
						}

					}

					/*	printf("%d, %s.%.3s, %d, %d\n", count, dirName,
					 recoveryEntry->entry.DIRNameExt,
					 recoveryEntry->entry.DIRFileSize,
					 startingClusterNumber);*/
					count++;
				}

			}
			seekPos = lseek(fd, 0, SEEK_CUR);
		} while (seekPos + sizeof(struct DirEntry)
				<= this->getDataOffsetByClusterNo(clusterNo + 1));

		int nextCluster = this->fat->addresses[clusterNo];
		if (nextCluster > 0x0 && nextCluster <= 0x0ffffff7) {
			hasNext = true;
			//printf("%d 's next cluster is %d\n", clusterNo, nextCluster);
			clusterNo = nextCluster;
		} else {
			hasNext = false;
		}

	} while (hasNext);

//start recovering
	if (foundEntry.size() > 1 && !this->request->flags['m']) {
		// more than one entry found
		printf("%s: error - ambiguous\n",
				this->request->flags['r'] ?
						this->request->shortFileName :
						this->request->longFileName);
	} else if (foundEntry.size() == 0) {
		// no entry found
		printf("%s: error - file not found\n",
				this->request->flags['r'] ?
						this->request->shortFileName :
						this->request->longFileName);
	} else {
		// only one, start recovering
		//	printf("Size: %d\n", foundEntry.size());
		RecoveryEntry* recover = foundEntry.at(0);

		//	printf("debug breakpoint \n");
		//	return;
		int ClusterNumber = this->getClusterNoFromHiLow(
				recover->entry.DIRFstClusHI, recover->entry.DIRFstClusLO);

		/*	printf("ClustNumber: %d %x \n", ClusterNumber,
		 this->fat->addresses[ClusterNumber]);
		 */
		// change fat value to valid.
		if (ClusterNumber == 0 || this->fat->addresses[ClusterNumber] == 0x00) {

			lseek(this->fd, recover->pos, SEEK_SET);
			write(this->fd, &firstCharacterOfFileName, 1);
			//		printf("[DEBUG] writing %c to %d\n", firstCharacterOfFileName,	recover->pos);
			if (ClusterNumber != 0) {
				//we don't need to update FAT if clusternumber is 0
				for (n = 0; n < this->boot->BPBNumFATs; n++) {
					lseek(this->fd,
							this->boot->BPBBytsPerSec

									* (this->boot->BPBRsvdSecCnt
											+ n * this->boot->BPBFATSz32)
									+ (ClusterNumber * 4), SEEK_SET);
					int eoc = 0x0fffffff;
					write(this->fd, &eoc, 4);
				/*				printf("[DEBUG] writing EOC to %d\n",
					 this->boot->BPBBytsPerSec * this->boot->BPBSecPerClus
					 * (this->boot->BPBRsvdSecCnt
					 + n * this->boot->BPBFATSz32)
					 + (ClusterNumber * 4));*/
				}
			}
			//update LFN
			int LFNSize = recover->LFNStackPos.size();
			//printf("breakpoint\n");
			//return;
			while (recover->LFNStackPos.size() != 0) {

				int lfnPos = recover->LFNStackPos.back();
				recover->LFNStackPos.pop_back();
				unsigned char sequenceNumber = LFNSize
						- recover->LFNStackPos.size();
				if (recover->LFNStackPos.size() == 0) {
					sequenceNumber += 64;
				}
				lseek(this->fd, lfnPos, SEEK_SET);
				write(this->fd, &sequenceNumber, 1);
				//	printf("[DEBUG] writing %d to %d\n", sequenceNumber, lfnPos);
			}

			this->fat->addresses[ClusterNumber] = 0x0ffffff;

			//print message
			if (this->request->flags['r']) {
				if (this->request->flags['m']) {
					printf("%s: recovered with MD5\n",
							this->request->shortFileName);
				} else {
					printf("%s: recovered\n", this->request->shortFileName);
				}
			} else if (this->request->flags['R']) {
				printf("%s: recovered\n", this->request->longFileName);
			}

		} else {
			printf("%s: error - fail to recover\n",
					this->request->flags['r'] ?
							this->request->shortFileName :
							this->request->longFileName);
		}

	}
}

char* Recover::Core::getMD5(struct DirEntry file, char* md5String) {
	unsigned char* md5digest = (unsigned char*) calloc(MD5_DIGEST_LENGTH,
			sizeof(char));

//entry.DIRFileSize
	int clusterNo = this->getClusterNoFromHiLow(file.DIRFstClusHI,
			file.DIRFstClusLO);

	unsigned char* content = new unsigned char[file.DIRFileSize];
	if (clusterNo >= 0) {
		int seekPos = lseek(fd, 0, SEEK_CUR);
		lseek(this->fd, this->getDataOffsetByClusterNo(clusterNo),
		SEEK_SET);
		read(this->fd, content, file.DIRFileSize);
		lseek(fd, seekPos, SEEK_SET);
		/* Generate Digest */
		MD5(content, file.DIRFileSize, md5digest);
	} else {
		unsigned char* empty = new unsigned char[1];
		empty[0] = 0;
		MD5(empty, file.DIRFileSize, md5digest);
	}

//assume that size of content is less than one cluster

	//restore set pos

	/* Print Digest */
	for (int i = 0; i < 16; ++i)
		sprintf(&md5String[i * 2], "%02x", (unsigned int) md5digest[i]);

	//printf("[DEBUG] MD5 = %s\n", md5String);
	return md5String;
}
