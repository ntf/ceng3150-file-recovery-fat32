#ifndef Recover_H
#define Recover_H

#include <vector>
#include <map>
#include "fat.h"

#pragma pack(push,1)
struct BootEntry {
	unsigned char BSjmpBoot[3]; /* Assembly instruction to jump to boot code */
	unsigned char BSOEMName[8]; /* OEM Name in ASCII */
	unsigned short BPBBytsPerSec; /* Bytes per sector. Allowed values include 512,
	 1024, 2048, and 4096 */
	unsigned char BPBSecPerClus; /* Sectors per cluster (data unit). Allowed values are
	 powers of 2, but the cluster size must be 32KB or
	 smaller */
	unsigned short BPBRsvdSecCnt; /* Size in sectors of the reserved area */
	unsigned char BPBNumFATs; /* Number of FATs */
	unsigned short BPBRootEntCnt; /* Maximum number of files in the root directory for
	 FAT12 and FAT16. This is 0 for FAT32 */
	unsigned short BPBTotSec16; /* 16-bit value of number of sectors in file system */
	unsigned char BPBMedia; /* Media type */
	unsigned short BPBFATSz16; /* 16-bit size in sectors of each FAT for FAT12 and FAT16.
	 For FAT32, this field is 0 */
	unsigned short BPBSecPerTrk; /* Sectors per track of storage device */
	unsigned short BPBNumHeads; /* Number of heads in storage device */
	unsigned long BPBHiddSec; /* Number of sectors before the start of partition */
	unsigned long BPBTotSec32; /* 32-bit value of number of sectors in file system.
	 Either this value or the 16-bit value above must be
	 0 , sector size of FAT */
	unsigned long BPBFATSz32; /* 32-bit size in sectors of one FAT */
	unsigned short BPBExtFlags; /* A flag for FAT */
	unsigned short BPBFSVer; /* The major and minor version number */
	unsigned long BPBRootClus; /* Cluster where the root directory can be found */
	unsigned short BPBFSInfo; /* Sector where FSINFO structure can be found */
	unsigned short BPBBkBootSec; /* Sector where backup copy of boot sector is located */
	unsigned char BPBReserved[12]; /* Reserved */
	unsigned char BSDrvNum; /* BIOS INT13h drive number */
	unsigned char BSReserved1; /* Not used */
	unsigned char BSBootSig; /* Extended boot signature to identify if the next three
	 values are valid */
	unsigned long BSVolID; /* Volume serial number */
	unsigned char BSVolLab[11]; /* Volume label in ASCII. User defines when creating the
	 file system */
	unsigned char BSFilSysType[8]; /* File system type label in ASCII */
};
#pragma pack(pop)

#pragma pack(push,1)
struct DirEntry {
	unsigned char DIRName[8];/* File name */
	unsigned char DIRNameExt[3]; /* File name ext */
	unsigned char DIRAttr; /* File attributes */
	unsigned char DIRNTRes; /* Reserved */
	unsigned char DIRCrtTimeTenth;/* Created time (tenths of second) */
	unsigned short DIRCrtTime; /* Created time (hours, minutes, seconds) */
	unsigned short DIRCrtDate; /* Created day */
	unsigned short DIRLstAccDate; /* Accessed day */
	unsigned short DIRFstClusHI; /* High 2 bytes of the first cluster address */
	unsigned short DIRWrtTime; /* Written time (hours, minutes, seconds */
	unsigned short DIRWrtDate; /* Written day */
	unsigned short DIRFstClusLO; /* Low 2 bytes of the first cluster address */
	unsigned long DIRFileSize; /* File size in bytes. (0 for directories) */
};
#pragma pack(pop)

#pragma pack(push,1)
struct LFNEntry {
	unsigned char sequenceNumber;
	unsigned char Name1[10];
	unsigned char Attr;
	unsigned char Reserved1;
	unsigned char Checksum;
	unsigned char Name2[12];
	unsigned char Reserved2[2];
	unsigned char Name3[4];
};
#pragma pack(pop)



struct RecoveryEntry {
	struct Recover::DirEntry entry;
	int pos;
	std::vector<Recover::LFNEntry> LFNStack;
	std::vector<int> LFNStackPos;

};


namespace Recover {
class Request {
public:
	std::map<char, bool> flags;
	char* deviceFileName;
	char* shortFileName;
	char* longFileName;
	char* md5;
	Request();
	bool parse();
};

class Core {

public:
	void handle(Request* req);
	void printFileSystemInfo();
	void listAllDirectoryEntries();
	void recoverEntry();

protected:
	Request* request;
	void openDevice(int mode);
	int getDataOffsetByClusterNo(int cluster);
	std::string getLongFileName(std::vector<Recover::LFNEntry> LFNStack);
	int getClusterNoFromHiLow(unsigned short hi, unsigned short shortlow);

	char* getMD5(struct DirEntry file,char* md5String);
	int hasNext(int cluster);
	int fd; /* File descriptor */
	BootEntry* boot;
	FAT* fat;
};

}
#endif
