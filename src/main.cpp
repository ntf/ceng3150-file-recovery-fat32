#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <iostream>
#include "core.h"
#include <sys/stat.h> 
#include <fcntl.h>

using namespace std;

int main(int argc, char *argv[]) {

	/*
	 Usage: ./recover -d [device filename] [other arguments]
	 -i Print boot sector information
	 -l List all the directory entries
	 -r filename [-m md5] File recovery with 8.3 filename
	 -R filename File recovery with long filename
	 */
	Recover::Request* req = new Recover::Request();

	try {

		if (argc <= 1) {
			throw "not enough argument";
		}

		int flagCount = 0;
		for (int i = 1; i < argc; i++) {
			if (strcmp(argv[i], "-d") == 0) {
				req->flags['d'] = true;
				flagCount++;
				if (i + 1 < argc) {

					req->deviceFileName = new char[strlen(argv[i + 1]) + 2];
					strcpy(req->deviceFileName, argv[i + 1]);
					i++;

				} else {
					throw "device file name not exists";
				}
			} else if (strcmp(argv[i], "-i") == 0) {
				req->flags['i'] = true;
				flagCount++;
			} else if (strcmp(argv[i], "-l") == 0) {
				req->flags['l'] = true;
				flagCount++;
			} else if (strcmp(argv[i], "-r") == 0) {
				req->flags['r'] = true;
				flagCount++;
				if (i + 1 < argc) {
					req->shortFileName = new char[strlen(argv[i + 1]) + 2];
					strcpy(req->shortFileName, argv[i + 1]);

					i++;
				} else {
					throw "8.3 file name not exists";
				}
			} else if (strcmp(argv[i], "-m") == 0) {
				req->flags['m'] = true;
				if (i + 1 < argc) {
					req->md5 = new char[strlen(argv[i + 1]) + 2];
					strcpy(req->md5, argv[i + 1]);
					i++;
				}
			} else if (strcmp(argv[i], "-R") == 0) {
				req->flags['R'] = true;
				flagCount++;
				if (i + 1 < argc) {
					req->longFileName = new char[strlen(argv[i + 1]) + 2];
					strcpy(req->longFileName, argv[i + 1]);

					i++;
				} else {
					throw "long file name not exists";
				}

			} else {
				throw "argument error";
			}

		}

		/*
		 ��[other arguments]��. There can be 5 possible combinations, either:
		 -i -l -r filename
		 -r filename -m md5 -R filename
		 */
		if (!req->flags['d']) {
			throw "expected flag -d";
		}
		if (flagCount > 2) {
			throw "too many flags";
		}
		if (req->flags['m'] && !req->flags['r']) {
			throw "-m without -r";
		}

		//start
		/*
		printf("=====Input Dump=====\n");
		printf("-d: %d , -i : %d , -l : %d , -r : %d , -m : %d , -R : %d \n",
				req->flags['d'], req->flags['i'], req->flags['l'],
				req->flags['r'], req->flags['m'], req->flags['R']);
		printf("Device File Name : %s \n", req->deviceFileName);
		printf("8.3 FileName : %s \n", req->shortFileName);
		printf("Long File Name : %s\n", req->longFileName);
		printf("MD5 : %s\n", req->md5);
		printf("====DUMP END====\n");
		*/
		Recover::Core core;
		core.handle(req);

		//printf("====END======\n");
		return 0;
	} catch (const char* e) {
		//printf("[DEBUG] %s\n", e);
	}

	printf("Usage: ./recover -d [device filename] [other arguments]\n");
	printf("-i Print boot sector information\n");
	printf("-l List all the directory entries\n");
	printf("-r filename [-m md5] File recovery with 8.3 filename\n");
	printf("-R filename File recovery with long filename\n");

	return 0;
}
