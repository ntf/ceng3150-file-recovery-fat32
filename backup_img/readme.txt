m1_3.img normal image with no deletion
m4.img shortFileName deletion. Deleted: MAKEFILE, HELLO.TXT
m5.img check ambiguous. Deleted: HELLO.TXT, SELLO.TXT
m6.img longFileName deletion. Deleted: "Sea of Sorrows.unknown", "Sanctum of Rall is the best World.longextname"
m7.img check ambiguous with md5 Deleted: 1ea6fd2d0b017300b1e0b9fc4973d74d  HELLO.TXT, c1f1883d22cf0cb27f6cf1a402771dfc  SELLO.TXT

empty.img check empty file deletion with md5 ambiguous checking
d41d8cd98f00b204e9800998ecf8427e  EMPTY.TXT
db46e1b7b1ec92af4ce026e4fa611eab  AMPTY.TXT

