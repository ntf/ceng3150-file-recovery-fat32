#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <exception>

#include <iostream>
#include <string>
#include <vector>
#include <map>

#include <sys/stat.h>
#include <fcntl.h>
#include "fat.h"
using namespace std;
using namespace Recover;
void Recover::FAT::setFileDescriptor(int fd) {
	this->fd = fd;
}

void Recover::FAT::initialize(BootEntry* boot) {
	this->boot = boot;
	this->start = this->boot->BPBRsvdSecCnt * this->boot->BPBBytsPerSec;
	this->end = this->boot->BPBBytsPerSec
			* (this->boot->BPBRsvdSecCnt + this->boot->BPBFATSz32);
	this->addressSize = 4;


	int size = this->boot->BPBFATSz32 * this->boot->BPBBytsPerSec;
	this->size = this->boot->BPBFATSz32;

//	printf("[DEBUG] FAT START : %d ; END : %d ; size : %d; memeory size: %d \n",this->start,this->end,this->boot->BPBFATSz32,size);

	this->addresses = new unsigned int[size / 4];
	if (sizeof(int) != 4) {
		printf("[DEBUG][ERROR] sizeof(int)!=4\n");
	}

	//cluster 0 and cluster 1 are reserved
	//cluster 0 holds the FAT ID
	//cluster 1 nominally stores the end-of-cluster-chain
	lseek(fd, this->start, SEEK_SET);
	read(fd, this->addresses, size);

}
